module az.views.view;

import az.models.model;

/**
 * Base interface for view.
 */
interface View {
    @property Model style();
    @property Model data();
    @property Model selection();
}